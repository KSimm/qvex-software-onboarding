#!/usr/bin/env bash

echo "Setting up Environment..."

echo "Installing the 'rustlings' executable..."
cargo install --force --path .

if ! [ -x "$(command -v rustlings)" ]
then
    echo "WARNING: Please check that you have '$CargoBin' in your PATH environment variable!"
fi

# Checking whether Clippy is installed.
# Due to a bug in Cargo, this must be done with Rustup: https://github.com/rust-lang/rustup/issues/1514
Clippy=$(rustup component list | grep "clippy" | grep "installed")
if [ -z "$Clippy" ]
then
    echo "Installing the 'cargo-clippy' executable..."
    rustup component add clippy
fi

echo "All done! Run 'rustlings' to get started."

if [ `git remote get-url origin` == "https://gitlab.com/qvex/qvex-software-onboarding.git" ]; then
    RED='\033[0;31m'
    NC='\033[0m'
    printf "\n${RED}ERROR: This Repo is not a fork!\n\tMake sure you follow the video and create a fork:${NC}\n\thttps://youtu.be/rTuIunr_wWs.\n\n"
fi