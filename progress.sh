#!/bin/sh

LIST=`rustlings list | tr '[:space:]' '[\n*]'`

echo $LIST

DONE_COUNT=`echo $LIST | grep -ow Done | wc -l`
PENDING_COUNT=`echo $LIST | grep -ow Pending | wc -l`

echo "{ \"done\": $DONE_COUNT, \"pending\": $PENDING_COUNT }" > progress.json